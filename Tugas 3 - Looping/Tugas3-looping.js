console.log('---------no1---------')
console.log ('LOOPING PERTAMA')
var angka = 2;
while (angka <=20) 
{
    if (angka % 2 === 0){
    console.log(angka + ' - I Love coding');
    }
    angka++
}
console.log ('LOOPING KEDUA')
while (angka >= 2) 
{
    if(angka % 2 === 0)  {   
    console.log (angka + ' - I will become a mobile developer');
    }
    angka--
}
console.log('---------no1---------')

console.log('---------no2---------')
var i = 1;
for (i = 1; i < 21; i++) {
    if ((i % 3 === 0) && (i % 2 !== 0)) {
        console.log(i + ' - I Love Coding')
    }
    else if (i % 2 !== 0) {
        console.log(i + ' - Santai')
    }
    else if (i % 2 === 0) {
        console.log(i + ' - Berkualitas')
    }
}
console.log('---------no2---------')

console.log('---------no3---------')
var i = 1;
for (i = 0; i < 4; i++) 
{
    console.log('########')
}
console.log('---------no3---------')

console.log('---------no4---------')
var tangga = '#';
for (var deret = 1; deret <= 7; deret++)
{
  console.log(tangga)
  tangga =tangga + '#'
}
console.log('---------no4---------')

console.log('---------no5---------')
var i = 0
for (i = 0; i <= 7; i++) {
    if (i % 2 === 0) {
        console.log(' # # # #')
    }
    else {
        console.log('# # # #')
    }
}
console.log('---------no5---------')